#include "A_Client.h"

uint64_t A_Client::add_device(uint64_t device_id, const std::string& name, uint64_t value, uint32_t value1,
	uint32_t value2, uint32_t value3)
{
	A_Client_Device* client_device = (A_Client_Device*) new A_Client_Device;
	if (client_device != nullptr)
	{
		client_device->set_value(value1);
		client_device->set_value2(value2);
		client_device->set_value3(value3);
		client_device->set_device_id(device_id);
		client_device->set_name(name);
		Client::m_client_device_list.push_back(client_device);
		// Add device to base class
		return Client::add_device(device_id, Client_Type::A, value);
	}
	return 0;		// 0 indicates an error
}
