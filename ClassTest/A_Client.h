#pragma once
#include "Client.h"
#include "A_Client_Device.h"

class A_Client :
	public Client
{
public:
	A_Client()							{a_value = 0;}
	uint64_t							get_value(void)							{return a_value;}
	void								set_value(uint64_t value)				{a_value = value;}
	uint64_t							add_device(uint64_t device_id, const std::string& name, uint64_t value,
											uint32_t value1, uint32_t value2, uint32_t value3);
	A_Client_Device*					find_device(uint64_t device_id)
															{return (A_Client_Device*)Client::find_device(device_id);}
private:
	uint64_t		a_value;
};

