#pragma once
#include "Client_Device.h"
#include "Client.h"

class A_Client_Device :
	public Client_Device
{
public:
	uint32_t							get_value(void)							{return a_client_value;}
	void								set_value(uint32_t value)				{a_client_value = value;}
	uint32_t							get_value2(void)						{return aa_client_value;}
	void								set_value2(uint32_t value)				{aa_client_value = value;}
	uint32_t							get_value3(void)						{return aaa_client_value;}
	void								set_value3(uint32_t value)				{aaa_client_value = value;}
	std::string							get_name(void)							{return a_name;}
	void								set_name(const std::string& name)		{a_name = name;}
	A_Client_Device*					find_device(uint64_t device_id)
											{return (A_Client_Device*)((Client*)m_parent)->find_device(device_id);}
private:
	std::string		a_name;
	uint32_t		a_client_value;
	uint32_t		aa_client_value;
	uint32_t		aaa_client_value;
};

