#include "B_Client.h"

uint64_t B_Client::add_device(uint64_t device_id, uint64_t value, uint64_t value1)
{
	B_Client_Device* client_device = (B_Client_Device*) new B_Client_Device;
	if (client_device != nullptr)
	{
		client_device->set_value(value1);
		client_device->set_device_id(device_id);
		m_client_device_list.push_back(client_device);
		// Add device to base class
		return Client::add_device(device_id, Client_Type::B, value);
	}
	return 0;		// 0 indicates an error
}
