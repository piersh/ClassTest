#pragma once
#include "Client.h"
#include "B_Client_Device.h"

class B_Client :
	public Client
{
public:
	B_Client()							{b_value = 0;}
	uint64_t							get_value(void)							{return b_value;}
	void								set_value(uint64_t value)				{b_value = value;}
	uint64_t							add_device(uint64_t device_id, uint64_t value, uint64_t value1);
	B_Client_Device*					find_device(uint64_t device_id)
															{return (B_Client_Device*)Client::find_device(device_id);}
private:
	uint64_t		b_value;
};

