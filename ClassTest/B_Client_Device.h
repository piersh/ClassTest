#pragma once
#include "Client_Device.h"
#include "Client.h"

class B_Client_Device :
	public Client_Device
{
public:
	uint64_t							get_value(void)							{return b_client_value;}
	void								set_value(uint64_t value)				{b_client_value = value;}
	B_Client_Device*					find_device(uint64_t device_id)
											{return (B_Client_Device*)((Client*)m_parent)->find_device(device_id);}
private:
	uint64_t		b_client_value;
};

