// ClassTest.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "A_Client.h"
#include "B_Client.h"
#include "A_Client_Device.h"
#include "B_Client_Device.h"

constexpr auto A_CLIENT_DEVICE_ID = 3;
constexpr auto B_CLIENT_DEVICE_ID = 7;

int main()
{
	A_Client a_client;
	if (a_client.add_device(A_CLIENT_DEVICE_ID, "My name is Michael Caine", 11, 22, 23, 24) != 0)
	{
		std::cout << "A Client stores " << a_client.get_value() << std::endl;

		A_Client_Device* a_client_device = a_client.find_device(A_CLIENT_DEVICE_ID);
		if (a_client_device != nullptr)
		{
			std::cout << "A Client Device stores " << a_client_device->get_value() << std::endl;
			std::cout << "AA Client Device stores " << a_client_device->get_value2() << std::endl;
			std::cout << "AAA Client Device stores " << a_client_device->get_value3() << std::endl;
			std::cout << "A Client DeviceID " << a_client_device->get_device_id() << std::endl;
			std::cout << "A name: " << a_client_device->get_name() << std::endl;
		}
	}

	B_Client b_client;
	if (b_client.add_device(B_CLIENT_DEVICE_ID, 33, 44) != 0)
	{
		std::cout << "B Client stores " << b_client.get_value() << std::endl;

		B_Client_Device* b_client_device = b_client.find_device(B_CLIENT_DEVICE_ID);
		if (b_client_device != nullptr)
		{
			std::cout << "B Client Device stores " << b_client_device->get_value() << std::endl;
			std::cout << "B Client DeviceID " << b_client_device->get_device_id() << std::endl;
		}
	}

	if (a_client.add_device(A_CLIENT_DEVICE_ID + 1, "Gonna name them Jackie and Wilson", 12, 222, 223, 224) != 0)
	{
		std::cout << "A Client stores " << a_client.get_value() << std::endl;

		A_Client_Device* a_client_device = a_client.find_device(A_CLIENT_DEVICE_ID + 1);
		if (a_client_device != nullptr)
		{
			std::cout << "A Client Device stores " << a_client_device->get_value() << std::endl;
			std::cout << "AA Client Device stores " << a_client_device->get_value2() << std::endl;
			std::cout << "AAA Client Device stores " << a_client_device->get_value3() << std::endl;
			std::cout << "A Client DeviceID " << a_client_device->get_device_id() << std::endl;
			std::cout << "A name: " << a_client_device->get_name() << std::endl;
		}
	}

	if (b_client.add_device(B_CLIENT_DEVICE_ID + 1, 133, 144) != 0)
	{
		std::cout << "B Client stores " << b_client.get_value() << std::endl;

		B_Client_Device* b_client_device = b_client.find_device(B_CLIENT_DEVICE_ID + 1);
		if (b_client_device != nullptr)
		{
			std::cout << "B Client Device stores " << b_client_device->get_value() << std::endl;
			std::cout << "B Client DeviceID " << b_client_device->get_device_id() << std::endl;
		}
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
