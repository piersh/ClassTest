#include "A_Client.h"
#include "B_Client.h"
#include "A_Client_Device.h"
#include "B_Client_Device.h"

uint64_t Client::add_device(uint64_t device_id, Client_Type client_type, uint64_t value)
{
	switch (client_type)
	{
		case Client_Type::A:
		{
			bool device_found = true;
			A_Client_Device* client_device = (A_Client_Device*)find_device(device_id);
			if (client_device == nullptr)					// If we couldn't fine a device, create a new one
			{
				device_found = false;
				client_device = (A_Client_Device*)new A_Client_Device;
			}
			if (client_device != nullptr)
			{
				client_device->set_parent(this);
				((A_Client*)(this))->set_value(value);
				if (device_found)
				{	// If this is a new device, add it to the list
					m_client_device_list.push_back(client_device);
				}
				return device_id;
			}
			break;
		}
		case Client_Type::B:
		{
			bool device_found = true;
			B_Client_Device* client_device = (B_Client_Device*)find_device(device_id);
			if (client_device == nullptr)					// If we couldn't fine a device, create a new one
			{
				device_found = false;
				client_device = (B_Client_Device*) new B_Client_Device;
			}
			if (client_device != nullptr)
			{
				client_device->set_parent(this);
				((B_Client*)(this))->set_value(value);
				if (device_found)
				{	// If this is a new device, add it to the list
					m_client_device_list.push_back(client_device);
				}
				return device_id;
			}
			break;
		}
	}
	return 0;		// 0 indicates an error
}

bool Client::remove_device(uint64_t device_id)
{
	Client_Device* client_device = find_device(device_id);
	if (client_device)
	{
		m_client_device_list.remove(client_device);
		delete client_device;
		return true;
	}
	return false;
}

Client_Device* Client::find_device(uint64_t device_id)
{
	std::list<Client_Device*>::iterator it;
	for (it = m_client_device_list.begin(); it != m_client_device_list.end(); it++)
	{
		if ((*it)->get_device_id() == device_id)
		{
			return *it;
		}
	}
	return nullptr;
}
