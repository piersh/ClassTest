#pragma once
#include <list>
#include "Client_Device.h"

class Client
{
public:
	uint64_t		add_device			(uint64_t device_id, Client_Type client_type, uint64_t value);
	bool			remove_device		(uint64_t device_id);
	Client_Device*	find_device			(uint64_t device_id);
protected:
	std::list<Client_Device*>			m_client_device_list;
};

