#pragma once
#include <stdint.h>
#include <string>

enum class Client_Type
{
	A,
	B,
};

class Client_Device
{
public:
	uint64_t		get_device_id(void)						{return m_device_id;}
	void			set_device_id(uint64_t device_id)		{m_device_id = device_id;}
	void			set_parent(void* parent)				{m_parent = parent;}
protected:
	void*			m_parent;
private:
	uint64_t		m_device_id;
};

